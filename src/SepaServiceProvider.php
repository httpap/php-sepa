<?php

namespace Httpap\Sepa;

use Illuminate\Support\ServiceProvider;

class SepaServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        // nothing yet
    }

    public function boot(): void
    {
        $this->publishViews();
        $this->registerMigrations();
        $this->registerRoutes();
    }

    protected function publishViews(): void
    {
        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/courier'),
        ]);
    }

    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(realpath(__DIR__.'/../database/migrations'));
    }

    protected function registerRoutes(): void
    {
        $this->loadRoutesFrom(realpath(__DIR__ . '/sepa-routes.php'));
    }
}
