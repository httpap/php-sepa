<?php

use Illuminate\Support\Facades\Route;
use Httpap\Sepa\Http\Controllers\ExecuteSolutionController;

Route::group([
    'as' => 'sepa.',
    'prefix' => config('sepa.housekeeping_endpoint_prefix'),
], function () {
    Route::get('health-check', HealthCheckController::class)->name('healthCheck');
});
