<?php

namespace Httpap\Sepa\Enum;

enum OrganizationEnum: string
{
    case ASF = 'Association française des sociétés financières';
    case BDF = 'Banque de France';
    case DBE = 'Banco de Espana';
    case BOE = ' Bank of England';
    case CLS = 'CSL';
    case DB = 'Deutsche Bundesbank';
    case ECB = 'ECB';
    case ESMA = 'ESMA';
    case EUC = 'Euroclear';
    case FFI = 'FFI';
    case GUF = 'GUF';
    case NEXO = 'NEXO';
    case PUK = 'Payments UK';
    case RUCMPG = ' RU-CMPG';
    case SMPG = 'SMPG';
    case SWITCH = 'SWITCH';
    case T2S = 'T2S';
    case TIETO = 'Tieto';



}
