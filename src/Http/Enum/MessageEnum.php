<?php

namespace Httpap\Sepa\Enum;

enum MessageEnum: string
{
    case ACMT = 'Account Management';
    case ADMI = 'Administration';
    case AUTH = 'Authorities';
    case CAAA = 'Acceptor to Acquirer Card Transactions';
    case CAAD = 'Card Administration';
    case CAAM = 'ATM Management';
    case CAFC = 'Fee collection';
    case CAFM = 'File Management';
    case CAFR = 'Fraud Reporting and Disposition';
    case CAIN = 'Acquirer to Issuer Card Transactions';
    case CAMT = 'Cash Management';
    case CANM = 'Network Management';
    case CASP = 'Sale to POI Card Transactions';
    case SASR = 'Settlement Reporting';
    case CATM = 'Terminal Management';
    case CATP = 'ATM Card Transactions';
    case COLR = 'Collateral Management';
    case FXTR = 'Foreign Exchange Trade';
    case HEAD = 'Business Application Header';
    case PACS = 'Payments Clearing and Settlement';
    case PAIN = 'Payments Initiation';
    case REDA = 'Reference Data';
    case REMT = 'Payments Remittance Advice';
    case SECL = 'Securities Clearing';
    case SEEV = 'Securities Events';
    case SEMT = 'Securities Management';
    case SESE = 'Securities Settlement';
    case SETR = 'Securities Trade';
    case TSIN = 'Trade Services Initiation';
    case TSMT = 'Trade Services Management';
    case TSRV = 'Trade Services';

}
